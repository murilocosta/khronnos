import { firebaseConfiguration } from './firebase.configuration';

export const environment = {
  production: true,
  firebase: firebaseConfiguration
};
