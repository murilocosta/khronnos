import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../auth/auth.service';
import { passwordConfirmationValidator } from '../shared/password-confirmation.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: FormGroup;
  nowLoading = false;

  constructor(private router: Router,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      passwordConfirmation: new FormControl('', [
        Validators.required
      ])
    }, {
      validators: passwordConfirmationValidator
    });
  }

  onSubmit() {
    this.nowLoading = true;
    const {email, password} = this.form.value;
    this.authService.register(email, password).then(this.handleSubmitSuccess);
  }

  handleSubmitSuccess = () => {
    this.nowLoading = false;
    this.router.navigate(['tasks', 'all']);
  }

  onNavigateToLogin() {
    this.router.navigate(['login']);
  }

  isPasswordConfirmationUpdated(formControl: FormControl) {
    return formControl.touched && formControl.dirty;
  }
}
