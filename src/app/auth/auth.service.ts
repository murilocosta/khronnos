import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { StorageService } from '../shared/storage.service';

@Injectable()
export class AuthService {
  private authKey = 'k-auth-uid';

  constructor(private firebaseAuth: AngularFireAuth,
              private storageService: StorageService) {
    this.firebaseAuth.auth.onAuthStateChanged((user) => {
      if (user) {
        this.storageService.store(this.authKey, {uid: user.uid});
      } else {
        this.storageService.destroy(this.authKey);
      }
    });
  }

  register(email: string, password: string) {
    return new Promise((resolve, reject) => {
      this.firebaseAuth.auth
        .createUserWithEmailAndPassword(email, password)
        .then(resolve)
        .catch(reject);
    });
  }

  signUp(email: string, password: string) {
    return new Promise((resolve, reject) => {
      this.firebaseAuth.auth
        .signInWithEmailAndPassword(email, password)
        .then(resolve)
        .catch(reject);
    });
  }

  signOut() {
    return this.firebaseAuth.auth.signOut();
  }

  getUserId() {
    const user = this.storageService.fetch(this.authKey);
    return user ? user.uid : null;
  }

  getUserResource(resource) {
    return `users/${this.getUserId()}/${resource}`;
  }
}
