import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Transition, TransitionController, TransitionDirection } from 'ng2-semantic-ui';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { TaskDetailService } from './task-detail.service';
import { getTaskPriorityIcon, Task } from '../task/task.model';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.css']
})
export class TaskDetailComponent implements OnInit, OnDestroy {
  private subscription: Subscription;

  private task$: Observable<any>;

  private taskDetailService: TaskDetailService;
  private transitionController = new TransitionController();

  constructor(private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.subscription = this.activatedRoute
      .data
      .subscribe(({taskDetailService}) => {
        this.transitionController.animate(new Transition('fade', 800, TransitionDirection.In));
        this.task$ = taskDetailService.fetchObservable();
        this.taskDetailService = taskDetailService;
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onSaveDescription(form: NgForm) {
    if (form.touched && form.dirty) {
      const taskDescription = form.controls['taskDescription'].value;
      if (taskDescription) {
        this.taskDetailService.updateDescription(taskDescription);
        form.resetForm({taskDescription});
      }
    }
  }

  onCloseTask(task: Task) {
    this.taskDetailService.updateClosed(!task.closed);
  }

  onDeleteTask() {
    this.taskDetailService.remove();
  }

  onRestoreTask() {
    this.taskDetailService.restore();
  }

  getPriorityIcon(priorityValue: string) {
    return getTaskPriorityIcon(priorityValue);
  }
}
