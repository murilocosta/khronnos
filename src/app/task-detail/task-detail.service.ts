import { AngularFireAction, AngularFireObject } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';

import { Task, TaskComment } from '../task/task.model';

export class TaskDetailService {
  private object: AngularFireObject<any>;

  constructor(object: AngularFireObject<any>) {
    this.object = object;
  }

  createComment(task: Task, content: string) {
    const current = Object.values(task.comments || {});
    return this.object.update({
      comments: current.concat([{content}])
    });
  }

  updateClosed(closed: boolean) {
    return this.object.update({
      closed,
      closedAt: closed ? new Date().getTime() : null
    });
  }

  updateDescription(description: string) {
    return this.object.update({description});
  }

  remove() {
    this.object.update({deleted: true});
  }

  restore() {
    this.object.update({deleted: false});
  }

  destroy() {
    return this.object.remove();
  }

  fetchObservable(): Observable<Task> {
    return this.object
      .snapshotChanges()
      .map(this.mapToObject)
      .publishReplay(1)
      .refCount();
  }

  mapToObject(object: AngularFireAction<any>) {
    return {
      ...object.payload.val(),
      uid: object.payload.key
    };
  }
}
