import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { TaskService } from '../task/task.service';
import { TaskDetailService } from './task-detail.service';

@Injectable()
export class TaskDetailResolver implements Resolve<TaskDetailService> {
  constructor(private taskService: TaskService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): TaskDetailService {
    return this.taskService.createObject(route.params['taskId']);
  }
}
