import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { getTaskCollectionColors } from '../task-collection/task-collection.model';
import { TaskCollectionListService } from '../task-collection/task-collection-list.service';

@Component({
  selector: 'app-task-collection-form',
  templateUrl: './task-collection-form.component.html',
  styleUrls: ['./task-collection-form.component.css']
})
export class TaskCollectionFormComponent implements OnInit {
  @Input() service: TaskCollectionListService;
  @Input() modalInstance: any;

  form: FormGroup;
  collectionColors = getTaskCollectionColors();

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      color: new FormControl(null)
    });
  }

  onSelectColor(color: string) {
    this.form.patchValue({color});
  }

  onSubmit() {
    const {name, color} = this.form.value;
    this.service
      .save({name, color})
      .then(() => {
        this.modalInstance.hide();
        this.form.reset();
      });
  }

  isColorSelected(color: string) {
    return this.form.get('color').value === color ? 'massive' : 'huge';
  }
}
