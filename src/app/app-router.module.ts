import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './auth/auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { TaskCollectionResolver } from './task-collection/task-collection.resolver';
import { TaskDetailResolver } from './task-detail/task-detail.resolver';
import { TaskListResolver } from './task-list/task-list.resolver';
import { TaskListByCollectionResolver } from './task-list/task-list-by-collection.resolver';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { TaskComponent } from './task/task.component';
import { FallbackComponent } from './fallback/fallback.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'tasks',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'tasks',
    canActivate: [
      AuthGuard
    ],
    component: DashboardComponent,
    resolve: {
      taskCollectionListService: TaskCollectionResolver
    },
    children: [
      {
        path: '',
        redirectTo: 'all',
        pathMatch: 'full'
      },
      {
        path: 'collections/:taskCollectionId',
        component: TaskComponent,
        resolve: {
          taskListService: TaskListByCollectionResolver,
          taskCollectionListService: TaskCollectionResolver
        },
        children: [
          {
            path: 'detail/:taskId',
            component: TaskDetailComponent,
            resolve: {
              taskDetailService: TaskDetailResolver
            }
          }
        ]
      },
      {
        path: ':action',
        component: TaskComponent,
        resolve: {
          taskListService: TaskListResolver,
          taskCollectionListService: TaskCollectionResolver
        },
        children: [
          {
            path: 'detail/:taskId',
            component: TaskDetailComponent,
            resolve: {
              taskDetailService: TaskDetailResolver
            }
          }
        ]
      }
    ]
  },
  {
    path: '**',
    component: FallbackComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule],
  providers: [
    AuthGuard,
    TaskCollectionResolver,
    TaskDetailResolver,
    TaskListResolver,
    TaskListByCollectionResolver
  ]
})
export class AppRouterModule {
}
