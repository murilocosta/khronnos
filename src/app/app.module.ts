import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { NgSemanticModule } from 'ng-semantic';
import { SuiModule } from 'ng2-semantic-ui';

import { AppRouterModule } from './app-router.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FallbackComponent } from './fallback/fallback.component';
import { LoginComponent } from './login/login.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { RegisterComponent } from './register/register.component';
import { TaskCollectionFormComponent } from './task-collection-form/task-collection-form.component';
import { TaskComponent } from './task/task.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { TaskFormComponent } from './task-form/task-form.component';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskGroupComponent } from './task-group/task-group.component';
import { TaskCommentComponent } from './task-comment/task-comment.component';
import { TaskCommentFormComponent } from './task-comment-form/task-comment-form.component';

import { AuthService } from './auth/auth.service';
import { StorageService } from './shared/storage.service';
import { TaskCollectionService } from './task-collection/task-collection.service';
import { TaskService } from './task/task.service';

import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    FallbackComponent,
    SideMenuComponent,
    TaskCollectionFormComponent,
    TaskComponent,
    TaskDetailComponent,
    TaskFormComponent,
    TaskListComponent,
    TaskGroupComponent,
    TaskCommentComponent,
    TaskCommentFormComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    NgSemanticModule,
    SuiModule,
    AppRouterModule
  ],
  providers: [
    AuthService,
    StorageService,
    TaskCollectionService,
    TaskService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
