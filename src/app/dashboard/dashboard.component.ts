import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { TaskCollectionListService } from '../task-collection/task-collection-list.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnDestroy {
  private subscription: Subscription;

  private taskCollectionListService: TaskCollectionListService;

  constructor(activatedRoute: ActivatedRoute) {
    this.subscription = activatedRoute
      .data
      .subscribe(({taskCollectionListService}) => {
        this.taskCollectionListService = taskCollectionListService;
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
