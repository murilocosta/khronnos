import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  nowLoading = false;
  form: FormGroup;

  constructor(private router: Router,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ])
    });
  }

  onSubmit() {
    this.nowLoading = true;
    const {email, password} = this.form.value;
    this.authService
      .signUp(email, password)
      .then(this.handleSubmitSuccess)
      .catch(this.handleSubmitError);
  }

  handleSubmitSuccess = () => {
    this.router
      .navigate(['tasks', 'all'])
      .then(() => {
        this.nowLoading = false;
      });
  }

  handleSubmitError = () => {
    this.nowLoading = false;
  }

  onNavigateToRegister() {
    this.router.navigate(['register']);
  }
}
