import { Component, EventEmitter, Input, Output } from '@angular/core';
import { head } from 'lodash';

import {
  getTaskPriorities,
  getTaskPriorityIcon,
  Task,
  TaskPriority,
  TaskPriorityItem,
  UpdateTaskCollectionEvent,
  UpdateTaskPriorityEvent
} from '../task/task.model';
import { TaskCollectionMap } from '../task-collection/task-collection.model';

@Component({
  selector: 'app-task-group',
  templateUrl: './task-group.component.html',
  styleUrls: ['./task-group.component.css']
})
export class TaskGroupComponent {
  @Input() icon: string;
  @Input() header: string;
  @Input() disabled = false;
  @Input() tasks: Task[];
  @Input() taskCollections: TaskCollectionMap;

  @Output() priorityUpdated = new EventEmitter<UpdateTaskPriorityEvent>();
  @Output() collectionUpdated = new EventEmitter<UpdateTaskCollectionEvent>();
  @Output() closed = new EventEmitter<Task>();
  @Output() deleted = new EventEmitter<Task>();
  @Output() restored = new EventEmitter<Task>();

  priorities: TaskPriorityItem[];
  isCollapsed = false;

  objectKeys = Object.keys;

  constructor() {
    this.priorities = getTaskPriorities();
  }

  onUpdatePriority({uid}: Task, priority: TaskPriority) {
    this.priorityUpdated.emit({uid, priority});
  }

  onUpdateCollection({uid}: Task, collectionUid: string) {
    this.collectionUpdated.emit({uid, collectionUid});
  }

  onCloseTask(task: Task) {
    this.closed.emit(task);
  }

  onDeleteTask(task: Task) {
    this.deleted.emit(task);
  }

  onRestoreTask(task: Task) {
    this.restored.emit(task);
  }

  onClickToggle() {
    this.isCollapsed = !this.isCollapsed;
  }

  getCollection(task: Task) {
    const {collections} = task;
    const key = collections ? head(this.objectKeys(task.collections)) : null;
    return key ? this.taskCollections[key] : null;
  }

  getPriorityIcon(priorityValue) {
    return getTaskPriorityIcon(priorityValue);
  }
}
