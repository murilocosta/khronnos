import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { property } from 'lodash';
import { Subscription } from 'rxjs/Subscription';

import {
  getTaskPriorities,
  getTaskPriorityIcon,
  Task,
  TaskPriority,
  TaskPriorityItem,
  TaskType
} from '../task/task.model';
import { getTaskListPageTitle } from '../task/task-action.model';
import { TaskCollectionMap } from '../task-collection/task-collection.model';
import { TaskListService } from '../task-list/task-list.service';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.css']
})
export class TaskFormComponent implements OnInit, OnChanges, OnDestroy {
  @Input() service: TaskListService;
  @Input() taskCollections: TaskCollectionMap;

  private routeParamsSubscription: Subscription;

  private form: FormGroup;
  private taskCollectionId: number;
  private defaultHeader: string;
  priorities: TaskPriorityItem[];
  objectKeys = Object.keys;

  _taskCollections = {};

  constructor(private activatedRoute: ActivatedRoute) {
    this.routeParamsSubscription = this.activatedRoute
      .params
      .subscribe((params) => {
        this.taskCollectionId = params['taskCollectionId'];
        this.defaultHeader = getTaskListPageTitle(params['action']);
      });
    this.priorities = getTaskPriorities();
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      title: new FormControl('', [
        Validators.required
      ]),
      priority: new FormControl(TaskPriority.None, [
        Validators.required
      ]),
      collections: new FormControl(null),
      dueAt: new FormControl(null)
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    const {taskCollections} = changes;
    if (taskCollections && taskCollections.currentValue) {
      this._taskCollections = taskCollections.currentValue;
    }
  }

  ngOnDestroy(): void {
    this.routeParamsSubscription.unsubscribe();
  }

  onSelectPriority(priority: string) {
    this.form.patchValue({priority});
  }

  onSelectCollection(uid: string) {
    this.form.patchValue({
      collections: {
        [uid]: true
      }
    });
  }

  onSubmit() {
    if (this.form.valid) {
      this.service
        .save(this.createTask(this.form.value))
        .then(() => {
          this.form.reset({priority: TaskPriority.None});
        });
    }
  }

  createTask({title, priority, collections, dueAt}): Task {
    return {
      type: TaskType.Simple,
      title,
      priority,
      collections: collections || null,
      createdAt: new Date().getTime(),
      dueAt: dueAt ? dueAt.getTime() : null
    };
  }

  getPageHeader() {
    return property([this.taskCollectionId, 'name'])(this._taskCollections) || this.defaultHeader;
  }

  getPriorityIcon(priorityValue) {
    return getTaskPriorityIcon(priorityValue);
  }

  isPrioritySelected(priority: string) {
    return this.form.get('priority').value === priority;
  }

  isCollectionSelected(uid: string) {
    return property(uid)(this.form.get('collections').value);
  }
}
