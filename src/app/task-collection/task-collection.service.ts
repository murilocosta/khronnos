import { Injectable } from '@angular/core';
import { AngularFireDatabase, QueryFn } from 'angularfire2/database';

import { AuthService } from '../auth/auth.service';
import { TaskCollectionListService } from './task-collection-list.service';

@Injectable()
export class TaskCollectionService {
  endpoint: string;

  constructor(private database: AngularFireDatabase,
              authService: AuthService) {
    this.endpoint = authService.getUserResource('collections');
  }

  createListService(query: QueryFn = null) {
    return new TaskCollectionListService(this.database.list(this.endpoint, query));
  }
}
