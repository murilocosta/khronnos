import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { TaskCollectionService } from './task-collection.service';
import { TaskCollectionListService } from './task-collection-list.service';

@Injectable()
export class TaskCollectionResolver implements Resolve<TaskCollectionListService> {
  constructor(private taskCollectionService: TaskCollectionService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): TaskCollectionListService {
    return this.taskCollectionService.createListService();
  }
}
