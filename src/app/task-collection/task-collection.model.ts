import { transform } from 'lodash';

export enum TaskCollectionColor {
  None = '',
  Red = 'red',
  Orange = 'orange',
  Yellow = 'yellow',
  Green = 'green',
  Blue = 'blue',
  Violet = 'violet',
  Purple = 'purple',
  Pink = 'pink',
  Brown = 'brown'
}

export interface TaskCollectionColorItem {
  label: string;
  value: string;
}

export class TaskCollection {
  public uid?: string;
  public name: string;
  public color?: TaskCollectionColor;
}

export interface TaskCollectionMap {
  [uid: string]: TaskCollection | any;
}

export function getTaskCollectionColors(): TaskCollectionColorItem {
  return transform(TaskCollectionColor, taskCollectionColorTransformer, []);

  function taskCollectionColorTransformer(result, value, key) {
    return result.push({label: key, value});
  }
}
