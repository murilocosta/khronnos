import { AngularFireAction, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';

import { TaskCollection, TaskCollectionMap } from './task-collection.model';

export class TaskCollectionListService {
  private list: AngularFireList<any>;

  constructor(list: AngularFireList<any>) {
    this.list = list;
  }

  save(taskCollection: TaskCollection) {
    return this.list.push(taskCollection);
  }

  update(taskCollection: TaskCollection) {
    const {uid, name, color} = taskCollection;
    return this.list.update(uid, {name, color});
  }

  fetchObservable(): Observable<TaskCollectionMap> {
    return this.list
      .snapshotChanges()
      .map(this.mapToObject)
      .publishReplay(1)
      .refCount();
  }

  mapToObject(actionList: AngularFireAction<any>[]) {
    return actionList.reduce(taskCollectionReducer, {});

    function taskCollectionReducer(taskCollections, action: AngularFireAction<any>) {
      return Object.assign({}, taskCollections, {
        [action.payload.key]: {
          ...action.payload.val()
        }
      });
    }
  }
}
