import { Component, Input } from "@angular/core";

import { Task } from "../task/task.model";

@Component({
    selector: 'app-task-comment',
    templateUrl: './task-comment.component.html',
    styleUrls: ['./task-comment.component.css']
})
export class TaskCommentComponent {
    @Input() task: Task;

    getTaskComments() {
        return Object.values(this.task.comments || {});
    }
}
