import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AuthService } from '../auth/auth.service';
import { TaskCollectionListService } from '../task-collection/task-collection-list.service';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {
  @Input() service: TaskCollectionListService;

  objectKeys = Object.keys;
  taskCollections$: Observable<any>;

  constructor(private router: Router,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.taskCollections$ = this.service.fetchObservable();
  }

  onLogout() {
    this.authService
      .signOut()
      .then(() => this.router.navigate(['/login']));
  }
}
