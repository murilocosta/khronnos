import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Task } from './task.model';
import { TaskCollectionMap } from '../task-collection/task-collection.model';
import { TaskListService } from '../task-list/task-list.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit, OnDestroy {
  private routeDataSubscription: Subscription;

  taskListService: TaskListService;
  tasks$: Observable<Task[]>;
  taskCollections$: Observable<TaskCollectionMap>;

  constructor(private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.routeDataSubscription = this.activatedRoute
      .data
      .subscribe(({taskListService, taskCollectionListService}) => {
        this.taskCollections$ = taskCollectionListService.fetchObservable();
        this.tasks$ = taskListService.fetchObservable();
        this.taskListService = taskListService;
      });
  }

  ngOnDestroy(): void {
    this.routeDataSubscription.unsubscribe();
  }
}
