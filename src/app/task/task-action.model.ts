export enum TaskAction {
  All = 'all',
  Today = 'today',
  Closed = 'closed',
  Trash = 'trash'
}

export function getTaskListPageTitle(action: string) {
  switch (action) {
    case TaskAction.Today:
      return 'Today';
    case TaskAction.Closed:
      return 'Closed';
    case TaskAction.Trash:
      return 'Trash';
    case TaskAction.All:
      return 'All Tasks';
    default:
      return null;
  }
}
