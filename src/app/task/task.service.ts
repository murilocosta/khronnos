import { Injectable } from '@angular/core';
import { AngularFireDatabase, QueryFn } from 'angularfire2/database';

import { AuthService } from '../auth/auth.service';
import { TaskListService } from '../task-list/task-list.service';
import { TaskDetailService } from '../task-detail/task-detail.service';

@Injectable()
export class TaskService {
  endpoint: string;

  constructor(private database: AngularFireDatabase,
              authService: AuthService) {
    this.endpoint = authService.getUserResource('tasks');
  }

  createList(query: QueryFn = null) {
    return new TaskListService(this.database.list(this.endpoint, query));
  }

  createObject(taskId: string) {
    return new TaskDetailService(this.database.object(`${this.endpoint}/${taskId}`));
  }
}
