import { transform } from 'lodash';

export enum TaskType {
  Simple = 'SIMPLE',
  List = 'LIST'
}

export enum TaskPriority {
  High = 'HIGH',
  Medium = 'MEDIUM',
  Low = 'LOW',
  None = 'NONE'
}

export interface TaskPriorityItem {
  label: string;
  value: string;
}

export interface UpdateTaskPriorityEvent {
  uid: string;
  priority: TaskPriority;
}

export interface UpdateTaskCollectionEvent {
  uid: string;
  collectionUid: string;
}

export class TaskComment {
  public uid?: string;
  public content: string;
}

export class Task {
  public uid?: string;
  public type: TaskType;
  public title: string;
  public description?: string;
  public priority: TaskPriority;
  public collections?: { [uid: string]: boolean };
  public comments?: TaskComment[];
  public createdAt: number | Date;
  public dueAt?: number | Date;
  public closedAt?: number | Date;
  public closed?: boolean;
  public deleted?: boolean;
}

export function getTaskPriorities(): TaskPriorityItem[] {
  return transform(TaskPriority, taskPriorityTransformer, []);

  function taskPriorityTransformer(result, value, key) {
    return result.push({label: key, value});
  }
}

export function getTaskPriorityIcon(priorityValue: string) {
  switch (priorityValue) {
    case TaskPriority.High:
      return 'pink thermometer full icon';
    case TaskPriority.Medium:
      return 'orange thermometer half icon';
    case TaskPriority.Low:
      return 'blue thermometer quarter icon';
    case TaskPriority.None:
      return 'disabled thermometer empty icon';
    default:
      return '';
  }
}
