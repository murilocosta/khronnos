import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { QueryFn } from 'angularfire2/database';

import { TaskService } from '../task/task.service';
import { TaskAction } from '../task/task-action.model';
import { TaskListService } from './task-list.service';

@Injectable()
export class TaskListResolver implements Resolve<TaskListService> {
  constructor(private taskService: TaskService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): TaskListService {
    const actionType = route.params['action'];
    const query = this.createQuery(actionType);
    return this.taskService.createList(query);
  }

  private createQuery(actionType: string): QueryFn {
    const {All, Today, Closed, Trash} = TaskAction;

    switch (actionType) {
      case Today:
        return (fb) => {
          const today = new Date();
          today.setHours(0, 0, 0, 0);
          return fb.orderByChild('createdAt').startAt(today.getTime());
        };

      case Closed:
        return (fb) => {
          return fb.orderByChild('closed').equalTo(true);
        };

      case Trash:
        return (fb) => {
          return fb.orderByChild('deleted').equalTo(true);
        };

      case All:
      default:
        return null;
    }
  }
}
