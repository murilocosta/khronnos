import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

import { Task, TaskPriorityItem, UpdateTaskCollectionEvent, UpdateTaskPriorityEvent } from '../task/task.model';
import { TaskCollectionMap } from '../task-collection/task-collection.model';
import { TaskListService } from './task-list.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnChanges {
  @Input() service: TaskListService;
  @Input() tasks: Task[];
  @Input() taskCollections: TaskCollectionMap;

  tasksOpen: Task[] = [];
  tasksClosed: Task[] = [];
  tasksRemoved: Task[] = [];
  _taskCollections: TaskCollectionMap = {};

  ngOnChanges(changes: SimpleChanges): void {
    const {tasks, taskCollections} = changes;
    if (tasks && tasks.currentValue) {
      this.tasksOpen = [];
      this.tasksClosed = [];
      this.tasksRemoved = [];
      this.groupTasks(tasks.currentValue);
    }

    if (taskCollections && taskCollections.currentValue) {
      this._taskCollections = taskCollections.currentValue;
    }
  }

  groupTasks(tasks: Task[]) {
    tasks.forEach((task) => {
      const {closed, deleted} = task;
      if (deleted === true) {
        this.tasksRemoved.push(task);
      } else if (closed === true) {
        this.tasksClosed.push(task);
      } else {
        this.tasksOpen.push(task);
      }
    });
  }

  onUpdatePriority(taskEvent: UpdateTaskPriorityEvent) {
    this.service.updatePriority(taskEvent.uid, taskEvent.priority);
  }

  onUpdateCollection(taskEvent: UpdateTaskCollectionEvent) {
    this.service.updateCollection(taskEvent.uid, taskEvent.collectionUid);
  }

  onCloseTask({uid, closed}: Task) {
    this.service.updateClosed(uid, closed);
  }

  onDeleteTask({uid}: Task) {
    this.service.remove(uid);
  }

  onRestoreTask({uid}: Task) {
    this.service.restore(uid);
  }
}
