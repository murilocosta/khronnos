import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { QueryFn } from 'angularfire2/database';

import { TaskService } from '../task/task.service';
import { TaskListService } from './task-list.service';

@Injectable()
export class TaskListByCollectionResolver implements Resolve<TaskListService> {
  constructor(private taskService: TaskService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): TaskListService {
    const collectionId = route.params['taskCollectionId'];
    const query = this.createQuery(collectionId);
    return this.taskService.createList(query);
  }

  createQuery(collectionId: string): QueryFn {
    return (fb) => {
      return fb.orderByChild(`collections/${collectionId}`).equalTo(true);
    };
  }
}
