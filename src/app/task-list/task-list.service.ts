import { AngularFireAction, AngularFireList } from 'angularfire2/database';
import { omit } from 'lodash';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';

import { Task, TaskPriority } from '../task/task.model';

export class TaskListService {
  private list: AngularFireList<any>;

  constructor(list: AngularFireList<any>) {
    this.list = list;
  }

  save(task: Task) {
    return this.list.push(task);
  }

  updatePriority(uid: string, priority: TaskPriority) {
    return this.list.update(uid, {priority});
  }

  updateCollection(uid: string, collectionUid: string) {
    return this.list.update(uid, {
      collections: {
        [collectionUid]: true
      }
    });
  }

  updateClosed(uid: string, closed: boolean) {
    const shouldBeClosed = !closed;
    return this.list.update(uid, {
      closed: shouldBeClosed,
      closedAt: shouldBeClosed ? new Date().getTime() : null
    });
  }

  remove(uid: string) {
    return this.list.update(uid, {deleted: true});
  }

  restore(uid: string) {
    return this.list.update(uid, {deleted: false});
  }

  fetchObservable(): Observable<Task[]> {
    return this.list
      .snapshotChanges()
      .map(this.mapToList)
      .publishReplay(1)
      .refCount();
  }

  mapToList(actionList: AngularFireAction<any>[]) {
    return actionList.map(taskMapper);

    function taskMapper(action: AngularFireAction<any>) {
      return {...action.payload.val(), uid: action.payload.key};
    }
  }
}
