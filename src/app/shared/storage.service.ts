import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
  store(key: string, data: object) {
    const storageData = JSON.stringify(data);
    localStorage.setItem(key, storageData);
  }

  fetch(key: string): any {
    const storedData = localStorage.getItem(key);
    try {
      return JSON.parse(storedData);
    } catch (e) {

    }
    return storedData;
  }

  destroy(key: string) {
    localStorage.removeItem(key);
  }
}
