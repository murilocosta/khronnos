import { FormGroup, ValidationErrors } from '@angular/forms';

export function passwordConfirmationValidator(c: FormGroup): ValidationErrors | null {
  const password = c.get('password');
  const passwordConfirmation = c.get('passwordConfirmation');
  const exists = password && passwordConfirmation;
  return exists && password.value !== passwordConfirmation.value
    ? {passwordConfirmation: true}
    : null;
}
