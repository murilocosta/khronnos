import { Component, Input } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";

import { TaskDetailService } from "../task-detail/task-detail.service";
import { Task } from "../task/task.model";

@Component({
    selector: 'app-task-comment-form',
    templateUrl: './task-comment-form.component.html',
    styleUrls: ['./task-comment-form.component.css']
})
export class TaskCommentFormComponent {
    @Input() service: TaskDetailService;
    @Input() task: Task;
    @Input() modalInstance: any;

    private form: FormGroup;

    constructor() {
        this.form = new FormGroup({
            comment: new FormControl('', Validators.required)
        });
    }

    onSaveComment() {
        if (this.form.valid) {
            const commentContent = this.form.controls['comment'].value;
            this.service.createComment(this.task, commentContent).then(() => {
                this.modalInstance.hide();
                this.form.reset();
            });
        }
    }
}
